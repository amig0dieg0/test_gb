# Список значений
values = ["1", "2.5", "True"]

# Список типов
types = [int, float, bool, str]

# Преобразование значений к соответствующим типам
converted_values = list(map(lambda t, v: t(v), types, values))

print(converted_values)  # [1, 2.5, True, 'Hello']
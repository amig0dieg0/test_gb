import csv

# Задание 1
# имеется текстовый файл f.csv, по формату похожий на .csv с разделителем |
"""
lastname|name|patronymic|date_of_birth|id
Фамилия1|Имя1|Отчество1 |21.11.1998 |312040348-3048
Фамилия2|Имя2|Отчество2 |11.01.1972 |457865234-3431
...
"""
# 1. Реализовать сбор уникальных записей
# 2. Случается, что под одинаковым id присутствуют разные данные - собрать

def read_csv(file):
    unique_data = {}
    double_id = {}  

    with open(file, 'r', encoding='utf-8') as file:
        result = csv.DictReader(file, delimiter='|')
        for row in result:
            row = {k.strip(): v.strip() for k, v in row.items()}
            data = tuple(row[k] for k in row if k != 'id')  
            id = row['id']

            # собираем разные данные с одним id
            if id not in double_id:
                double_id[id] = set()
            double_id[id].add(data)
            
            # собираем уникальные данные
            if data not in unique_data:
                unique_data[data] = set()
            unique_data[data].add(id)

    '''Так как задание сформулировано очень не однозначно. 
        Принял решение собирать оникальные данные без учёта ID,
        и отдельно данные с одним ID '''

    print("Уникальные данные:")
    for data, ids in unique_data.items():
        print(f"{data} {ids}")

    print("Задвоенные ID:")
    for k,v in double_id.items():
        if len(v) > 1: 
            print(f"ID {k}:")
            for row in v:
                print(row)


read_csv('f.csv')
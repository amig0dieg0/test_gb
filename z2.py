# Задание 2


# в наличии список множеств. внутри множества целые числа

m = [{11, 3, 5}, {2, 17, 87, 32}, {4, 44}, {24, 11, 9, 7, 8}]
# посчитать
# 1. общее количество чисел

ok = sum([len(i) for i in m])
print(ok)

# 2. общую сумму чисел

os = sum([sum(i) for i in m])
print(os)

# 3. посчитать среднее значение

sz = sum([sum(i) for i in m]) / sum([len(i) for i in m])
print(sz)

# 4. собрать все числа из множеств в один кортеж

one_tuple = tuple(t for i in m for t in i )
print(one_tuple)

# *написать решения в одну строку
import os
from datetime import datetime, timedelta

# Задание 4
# Имеется папка с файлами
# Реализовать удаление файлов старше N дней

def delete_old_files(directory, N):
   
    now = datetime.now()
    print(now)
    
    for file in os.listdir(directory):

        file_path = os.path.join(directory, file)
        file_time = datetime.fromtimestamp(os.path.getmtime(file_path))
      
        if now - file_time > timedelta(days=N):
            print(timedelta(days=N))
            os.remove(file_path)
            print(f"Удалён файл: {file_path}")


delete_old_files('./delete', 0)
# Задание 5*
# Имеется текстовый файл с набором русских слов(имена существительные,
# им.падеж)
# Одна строка файла содержит одно слово.
# Написать программу которая выводит список слов, каждый элемент списка
# которого - это новое слово,
# которое состоит из двух сцепленных в одно, которые имеются в текстовом файле.
# Порядок вывода слов НЕ имеет значения
# Например, текстовый файл содержит слова: ласты, стык, стыковка, баласт,
# кабала, карась
# Пользователь вводмт первое слово: ласты
# Программа выводит:
# ластык
# ластыковка
# Пользователь вводмт первое слово: кабала
# Программа выводит:
# кабаласты
# кабаласт
# Пользователь вводмт первое слово: стыковка
# Программа выводит:
# стыковкабала
# стыковкарась

def read_file(file):
    with open(file, 'r', encoding='utf-8') as file:
        return [line.strip() for line in file]
    




def finish(new_slovo, spisok_slov):
    finish = []
    for slovo in spisok_slov:
        if slovo != new_slovo:
            for index in reversed(range(len(new_slovo))):
                res = new_slovo[:index] + slovo
                if new_slovo in res:
                    finish.append(res)
    return finish

def main():
    slova_from_file = read_file("z5.txt")
    vvod_slovo = input("Введите слово\n")
    print(finish(vvod_slovo, slova_from_file))

main()



              
        
        
    



            
            


    
# Задание 6*
# Имеется банковское API возвращающее JSON
'''{
"Columns": ["key1", "key2", "key3"],
"Description": "Банковское API каких-то важных документов",
"RowCount": 2,
"Rows": [
["value1", "value2", "value3"],
["value4", "value5", "value6"]
]
}'''
# Основной интерес представляют значения полей "Columns" и "Rows",
# которые соответственно являются списком названий столбцов и значениями
# столбцов
# Необходимо:
# 1. Получить JSON из внешнего API
# ендпоинт: GET https://api.gazprombank.ru/very/important/docs?
# documents_date={"начало дня сегодня в виде таймстемп"}
# (!) ендпоинт выдуманный
# 2. Валидировать входящий JSON используя модель pydantic
# (из ТЗ известно что поле "key1" имеет тип int, "key2"(datetime), "key3"(str))
# 2. Представить данные "Columns" и "Rows" в виде плоского csv-подобного
# pandas.DataFrame
# 3. В полученном DataFrame произвести переименование полей по след.
# маппингу
# "key1" -> "document_id", "key2" -> "document_dt", "key3" -> "document_name"
# 3. Полученный DataFrame обогатить доп. столбцом:
# "load_dt" -> значение "сейчас"(датавремя)

import requests
from datetime import datetime
from pydantic import BaseModel, validator, ValidationError
from typing import List, Any
import pandas as pd

def get_data():
    url = "https://api.gazprombank.ru/very/important/docs"
    today = datetime.now()
    start = datetime(today.year, today.month, today.day)
    timestamp = int(start.timestamp())
    params = {
        "documents_date": f"{timestamp}"
    }
    response = requests.get(url, params=params)
    data = response.json()
    return data

class RowModel(BaseModel):
    key1: int
    key2: datetime
    key3: str

class DocumentModel(BaseModel):
    Columns: List[str]
    Description: str
    RowCount: int
    Rows: List[RowModel]

    @validator('Rows', pre=True, each_item=True)
    def validate_rows(cls, v, values):
        columns = values['Columns']
        row_data = dict(zip(columns, v))
        return RowModel(
            key1=int(row_data[columns[0]]),
            key2=datetime.fromtimestamp(int(row_data[columns[1]])),
            key3=str(row_data[columns[2]])
        )


def process_data(data):
    try:
        document = DocumentModel(**data)
    except ValidationError as e:
        print("Validation error:", e)
        return None


    rows_data = [row.model_dump() for row in document.Rows]
    df = pd.DataFrame(rows_data)

 
    columns = data['Columns']
    column_mapping = {
        columns[0]: "document_id",
        columns[1]: "document_dt",
        columns[2]: "document_name"
    }
    df.rename(columns=column_mapping, inplace=True)
    df['load_dt'] = datetime.now()

    return df


# data = get_data()

data = {
    "Columns": ["key1", "key2", "key3"],
    "Description": "Банковское API каких-то важных документов",
    "RowCount": 2,
    "Rows": [[1, 1715828406, "value3"],[4, 1717878665, "value6"]]
    } 

df = process_data(data)
if df is not None:
    print(df)
